# СУБД PostgreSQL для бекенда
[[_TOC_]]
## Предварительные требования
На хосте должен быть установлен Docker

## На компьютере разработчика
### Создание образа в хранилище Gitlab
```shell
docker build -t registry.gitlab.com/teknokomo/vodonos/dbms-postgresql .
docker login registry.gitlab.com
docker push registry.gitlab.com/teknokomo/vodonos/dbms-postgresql
```
## На сервере
### Создание отдельной сети Docker для того чтобы контейнеры могли находить друг друга по именам контейнеров.
```shell
sudo docker network create -d bridge vodonos-network
```
### Создание хранилища для хранения динамических файлов БД вне контейнера на случай его сбоя
```shell
sudo docker volume create vodonos-postgresql-volume
sudo docker volume create vodonos-backup-volume
```
### Создание контейнера с СУБД
Замените пароль secret нормальным паролем
```shell
export POSTGRES_PASSWORD=secret
sudo docker run \
  --name vodonos-dbms-postgresql \
  --volume vodonos-postgresql-data-volume:/var/lib/postgresql/data \
  --volume vodonos-backup-volume:/backup \
  --env POSTGRES_DB=vodonos \
  --env POSTGRES_USER=user \
  --env POSTGRES_PASSWORD=$POSTGRES_PASSWORD \
  --publish 127.0.0.1:5432:5432 \
  --network vodonos-network \
  --detach \
  registry.gitlab.com/teknokomo/vodonos/dbms-postgresql
```
## В контейнере
### Подключение к контейнеру
```shell
sudo docker exec -it vodonos-dbms-postgresql bash
```
### Просмотр списка БД
```shell
psql -h 127.0.0.1 -U user --list
```
### Создание новой БД
```bash
createdb -h 127.0.0.1 -U user vodonos
```
### Восстановление БД из резервной копии
```shell
dropdb -h 127.0.0.1 -U user -d vodonos
psql -h 127.0.0.1 -U user -d vodonos < /backup/ГГГГ-ММ-ДД_ЧЧ-ММ-СС_vodonos_postgresql.sql
```
### Создание резервной копии БД
```shell
pg_dump -h 127.0.0.1 -U postgres -d vodonos > /backup/$(date +"%Y-%m-%d_%H-%M-%S")_vodonos_postgresql.sql
```
